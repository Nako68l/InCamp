/**
 * If this code works, it was written by Nako. If not, I don't know who wrote it..
 * 27.05.2017.
 */
public interface PunishmentInterface {
    void punish(int regime);
}

class SimplePunisher implements PunishmentInterface {

    @Override
    public void punish(int regime) {
        switch (regime) {
            case 1:
                System.out.println("Yellow lamp is shine");
                break;
            case 2:
                System.out.println("Orange lamp is shine");
                break;
            case 3:
                System.out.println("Red lamp is shine");
                break;
            case 4:
                System.out.println("BOOOOOOM");
                break;
            default:
                System.out.println("Just panel with lamps");
        }
    }
}
class ElectorPunisher implements PunishmentInterface {

    @Override
    public void punish(int regime) {
        switch (regime){
            case 1:
                System.out.println("You take damage from about 20W");
                break;
            case 2:
                System.out.println("You take damage from about 60W, be careful");
                break;
            case 3:
                System.out.println("Wow! 120W, the situation becomes dangerous");
                break;
            case 4:
                System.out.println("BAU! Death.....");
                break;
            default:
                System.out.println("Black spiked bracelet");
        }
    }
}