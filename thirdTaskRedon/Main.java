import java.util.ArrayList;

/**
 * If this code works, it was written by Nako. If not, I don't know who wrote it..
 * 24.05.2017.
 */
public class Main{
    public static void main(String[] args) {
        Raspberry ras1 = new Raspberry(214);
        Milk mil1 = new Milk(523);
        ElectorPunisher collar = new ElectorPunisher();
        SuperLibra sp = new SuperLibra(collar);
        CreateFood cake = new CreateFood("California Lemon Pound Cake", 300, 1);
        sp.add1ItemToLibra(cake);
        sp.add1ItemToLibra(ras1);
        sp.add1ItemToLibra(mil1);
        sp.add1ItemToLibra(mil1);

    }
}
//Regulator of punish regimes to which u can plug in one of punish tools
class PunishesRegulator {
    void takeCalValue(PunishmentInterface device, int calories){
        if (calories>1200 && calories < 2400){
            device.punish(1);
        } else if (calories>2400 && calories < 3600) {
            device.punish(2);
        }else if (calories>3600 && calories < 4700) {
            device.punish(3);
        }else if (calories>4700) {
            device.punish(4);
        }
    }
}
//Libra with punish regulator in which u can add items and it can tell u how many calories are inside of it
class SuperLibra {
    private final PunishmentInterface deviceToPunish;
    private PunishesRegulator pr = new PunishesRegulator();
    private ArrayList<Integer> stuffInLibra = new ArrayList<>();

    SuperLibra(PunishmentInterface dev){
        this.deviceToPunish = dev;
    }
    void add1ItemToLibra(Food obj){
        System.out.println("You have put to the libra " + obj.weight + " grams of " + obj.name);
        stuffInLibra.add(obj.calories*obj.weight);
        System.out.println("Now in the libra is " + summaryCalInLibra() + " calories");
        pr.takeCalValue(deviceToPunish, summaryCalInLibra());
    }
    private int summaryCalInLibra(){
        int calories = 0;
        for (Integer aStuffInLibra : stuffInLibra) {
            calories += aStuffInLibra;
        }
        return calories;
    }
}
//Just food
abstract class Food {
    int calories;
    int weight;
    String name;

    Food(String name,int weight,int calories){
        this.name = name;
        this.weight = weight;
        this.calories = calories;
        System.out.println("U have " + this.weight + " grams of "+ this.name);
    }

}
class CreateFood extends Food {
    CreateFood(String name,int weight,int calories){
        super(name, weight, calories);
    }
}

class Milk extends Food{
    Milk(int weight) {
        super("milk", weight, 4);}
}

class Raspberry extends Food {
    Raspberry(int weight) {
        super("raspberry", weight, 5);}
}