/**
 * If this code works, it was written by Nako. If not, I don't know who wrote it..
 * 24.05.2017.
 */
public class Main{
    public static void main(String[] args) {
        MilkRaspberryShake msh = new MilkRaspberryShake();
        System.out.println(msh.taste + "\n" + msh.scent + "\n" + msh.color);
    }
}

interface Food {
    boolean edible = true;
    String taste = "";
    String scent = "";
    String color = "";
}

class Milk implements Food{
    String taste = "milky";
    String scent = "";
    String color = "white";
}

class Raspberry implements Food{
    String taste = "sweet";
    String scent = "crimson";
    String color = "red";
}

class MilkRaspberryShake { //агрегация
    private Raspberry ingredient1 = new Raspberry();
    private Milk ingredient2 =  new Milk();
    String taste = "Taste of our cocktail is a mix of " + ingredient1.taste + " and " + ingredient2.taste;
    String scent = "Cocktail smells " + ingredient1.scent + " " + ingredient2.scent;
    String color = "Color of our cocktail is something between " + ingredient1.color + " and " + ingredient2.color;
}