import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Scanner;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;


class MonthCalendar {
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private DateTimeFormatter format = DateTimeFormatter.ofPattern("y-L-d");
    private static LocalDate chosenMonth = LocalDate.now();

    MonthCalendar() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the month number the calendar of which" +
                " you want to see, or press enter to see current one.. ");
        String input = scan.nextLine();
        String dateStr = Year.now() + "-" + input + "-" + MonthDay.now().getDayOfMonth();
        try {
            chosenMonth = LocalDate.parse(dateStr, format);
        } catch (Exception e) {
            chosenMonth = LocalDate.parse(LocalDate.now().toString(), format);
        }
        printCalendar();
    }

    private static void printCalendar() {
        LocalDate lastDayInMonth = chosenMonth.with(lastDayOfMonth()).plusDays(1);
        LocalDate monthDay = chosenMonth.withDayOfMonth(1);
        int firstDayIndex = monthDay.getDayOfWeek().getValue()-1;

        System.out.println("\t\t" + chosenMonth.getMonth());
        for (DayOfWeek day: DayOfWeek.values()){
            System.out.printf("%3s ",day.getDisplayName(TextStyle.SHORT,Locale.UK));
        }
        System.out.println();
        for (int i = 0; i < firstDayIndex; i++) {
            System.out.printf("    ");
        }
        while (monthDay.isBefore(lastDayInMonth)){
            DayOfWeek dayCaption = monthDay.getDayOfWeek();

            if (dayCaption == DayOfWeek.of(6) || dayCaption == DayOfWeek.of(7))
                System.out.print(ANSI_RED);
            if (monthDay.equals(LocalDate.now()))
                System.out.print(ANSI_BLUE);
            System.out.printf("%3d ", monthDay.getDayOfMonth());
            System.out.print(ANSI_RESET);

            if (dayCaption.getValue() == 7)
                System.out.println();
            monthDay = monthDay.plusDays(1);
        }
    }

    public static void main(String[] args) { new MonthCalendar(); }
}
